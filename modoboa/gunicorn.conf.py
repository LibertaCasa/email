backlog = 2048
bind = "unix:/opt/modoboa/gunicorn.sock"
pidfile = "/opt/modoboa/gunicorn.pid"
daemon = False
debug = True
workers = 2
logfile = "/var/log/modoboa/gunicorn.log"
loglevel = "debug"
